﻿using garden_designerAPI.Data;
using System.Data.Entity;
using System.Web.Http;

namespace garden_designerAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            Database.SetInitializer(new GardenDBInitializer());
            GardenDesignerDbContext context = new GardenDesignerDbContext();
            context.Database.Initialize(true);
            context.Database.CreateIfNotExists();
        }
    }
}
