﻿using garden_designerAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace garden_designerAPI.Dto
{
    public class ProjectStructureDetailsDto
    {
        public Structure Structure { get; set; }
        public int Amount { get; set; }
        public List<CoordinatesXY> Areas{ get; set; }
    }
}