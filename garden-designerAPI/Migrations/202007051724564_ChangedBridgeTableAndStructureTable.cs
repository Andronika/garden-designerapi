﻿namespace garden_designerAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedBridgeTableAndStructureTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CoordinatesXies", "Structure_StructureId", "dbo.Structures");
            DropIndex("dbo.CoordinatesXies", new[] { "Structure_StructureId" });
            AddColumn("dbo.ProjectStructures", "Amount", c => c.Int(nullable: false));
            AddColumn("dbo.CoordinatesXies", "ProjectStructure_ProjectStructureId", c => c.Int());
            CreateIndex("dbo.CoordinatesXies", "ProjectStructure_ProjectStructureId");
            AddForeignKey("dbo.CoordinatesXies", "ProjectStructure_ProjectStructureId", "dbo.ProjectStructures", "ProjectStructureId");
            DropColumn("dbo.Structures", "Amount");
            DropColumn("dbo.CoordinatesXies", "Structure_StructureId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CoordinatesXies", "Structure_StructureId", c => c.Int());
            AddColumn("dbo.Structures", "Amount", c => c.Int(nullable: false));
            DropForeignKey("dbo.CoordinatesXies", "ProjectStructure_ProjectStructureId", "dbo.ProjectStructures");
            DropIndex("dbo.CoordinatesXies", new[] { "ProjectStructure_ProjectStructureId" });
            DropColumn("dbo.CoordinatesXies", "ProjectStructure_ProjectStructureId");
            DropColumn("dbo.ProjectStructures", "Amount");
            CreateIndex("dbo.CoordinatesXies", "Structure_StructureId");
            AddForeignKey("dbo.CoordinatesXies", "Structure_StructureId", "dbo.Structures", "StructureId");
        }
    }
}
