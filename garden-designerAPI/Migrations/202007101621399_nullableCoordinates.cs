﻿namespace garden_designerAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullableCoordinates : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CoordinatesXies", "AreaX", c => c.Int());
            AlterColumn("dbo.CoordinatesXies", "AreaY", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CoordinatesXies", "AreaY", c => c.Int(nullable: false));
            AlterColumn("dbo.CoordinatesXies", "AreaX", c => c.Int(nullable: false));
        }
    }
}
