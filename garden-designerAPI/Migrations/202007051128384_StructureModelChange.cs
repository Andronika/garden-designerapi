﻿namespace garden_designerAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StructureModelChange : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CoordinatesXies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AreaX = c.Int(nullable: false),
                        AreaY = c.Int(nullable: false),
                        Structure_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Structures", t => t.Structure_Id)
                .Index(t => t.Structure_Id);
            
            AddColumn("dbo.Structures", "Amount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CoordinatesXies", "Structure_Id", "dbo.Structures");
            DropIndex("dbo.CoordinatesXies", new[] { "Structure_Id" });
            DropColumn("dbo.Structures", "Amount");
            DropTable("dbo.CoordinatesXies");
        }
    }
}
