﻿namespace garden_designerAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablesProjectProjectStructure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CoordinatesXies", "Structure_Id", "dbo.Structures");
            RenameColumn(table: "dbo.CoordinatesXies", name: "Structure_Id", newName: "Structure_StructureId");
            RenameIndex(table: "dbo.CoordinatesXies", name: "IX_Structure_Id", newName: "IX_Structure_StructureId");
            DropPrimaryKey("dbo.Structures");
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ProjectId);
            
            CreateTable(
                "dbo.ProjectStructures",
                c => new
                    {
                        ProjectStructureId = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        StructureId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectStructureId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.Structures", t => t.StructureId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.StructureId);

            DropColumn("dbo.Structures", "Id");
            AddColumn("dbo.Structures", "StructureId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Structures", "StructureId");
            AddForeignKey("dbo.CoordinatesXies", "Structure_StructureId", "dbo.Structures", "StructureId");
            
        }
        
        public override void Down()
        {
            AddColumn("dbo.Structures", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.CoordinatesXies", "Structure_StructureId", "dbo.Structures");
            DropForeignKey("dbo.ProjectStructures", "StructureId", "dbo.Structures");
            DropForeignKey("dbo.ProjectStructures", "ProjectId", "dbo.Projects");
            DropIndex("dbo.ProjectStructures", new[] { "StructureId" });
            DropIndex("dbo.ProjectStructures", new[] { "ProjectId" });
            DropPrimaryKey("dbo.Structures");
            DropColumn("dbo.Structures", "StructureId");
            DropTable("dbo.ProjectStructures");
            DropTable("dbo.Projects");
            AddPrimaryKey("dbo.Structures", "Id");
            RenameIndex(table: "dbo.CoordinatesXies", name: "IX_Structure_StructureId", newName: "IX_Structure_Id");
            RenameColumn(table: "dbo.CoordinatesXies", name: "Structure_StructureId", newName: "Structure_Id");
            AddForeignKey("dbo.CoordinatesXies", "Structure_Id", "dbo.Structures", "Id");
        }
    }
}
