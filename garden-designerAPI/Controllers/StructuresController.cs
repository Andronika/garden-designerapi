﻿using garden_designerAPI.Data;
using System.Linq;
using System.Web.Http;

namespace garden_designerAPI.Controllers
{
    [RoutePrefix("api/structures")]
    public class StructuresController : ApiController
    {
        GardenDesignerDbContext db = new GardenDesignerDbContext();

        [Route("")]
        public IHttpActionResult Get()
        {
            var structures = db.Structures;
            return Ok(structures);
        }

        [Route("types")]
        public IHttpActionResult GetTypes()
        {
            var structuresTypes = db.Structures.Select(s => s.Type).Distinct();
            return Ok(structuresTypes);
        }
    }
    
}
