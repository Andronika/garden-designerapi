﻿using garden_designerAPI.Data;
using garden_designerAPI.Dto;
using garden_designerAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace garden_designerAPI.Controllers
{

    [RoutePrefix("api/project")]
    public class ProjectController : ApiController
    {
        GardenDesignerDbContext db = new GardenDesignerDbContext();

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] List<ProjectStructureDetailsDto> structureDetailsDto)
        {
            var project = new Project() { Name = "Project" };
            var projectStructures = new List<ProjectStructure>() { };

            if (structureDetailsDto == null)
                return BadRequest();

            foreach(var structureDetails in structureDetailsDto)
            {
                var structure = db.Structures.FirstOrDefault(s => s.Name == structureDetails.Structure.Name);
                if (structure.StructureId == 0)
                    return NotFound();
                
                projectStructures.Add( new ProjectStructure()
                {
                    ProjectId = project.ProjectId,
                    StructureId = structure.StructureId,
                    Amount = structureDetails.Amount,
                    Areas = structureDetails.Areas
                });
            }
            
            project.ProjectStructures = projectStructures;
            db.Projects.Add(project);
            db.SaveChanges();
            return Created<Project>(Request.RequestUri, project);
        }
    }
}
