﻿using System.Collections.Generic;

namespace garden_designerAPI.Models
{
    public class Project
    {
        public  int ProjectId { get; set; }
        public string Name { get; set; }
        public List<ProjectStructure> ProjectStructures{ get; set; }
    }
}