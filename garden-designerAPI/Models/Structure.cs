﻿using System.Collections.Generic;

namespace garden_designerAPI.Models
{
    public class Structure
    {
        public int StructureId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public List<ProjectStructure> ProjectStructures { get; set; }
    }
}