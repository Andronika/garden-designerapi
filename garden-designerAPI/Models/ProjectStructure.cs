﻿using System.Collections.Generic;

namespace garden_designerAPI.Models
{
    public class ProjectStructure
    {
        public int ProjectStructureId { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int StructureId { get; set; }
        public Structure Structure { get; set; }
        public int Amount { get; set; }
        public List<CoordinatesXY> Areas { get; set; }

    }
}