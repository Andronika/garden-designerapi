﻿namespace garden_designerAPI.Models
{
    public class CoordinatesXY
    {
        public int Id { get; set; }
        public int? AreaX{ get; set; }
        public int? AreaY { get; set; }
    }
}