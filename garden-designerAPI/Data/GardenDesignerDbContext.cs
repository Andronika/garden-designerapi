﻿using garden_designerAPI.Models;
using System.Data.Entity;

namespace garden_designerAPI.Data
{
    public class GardenDesignerDbContext : DbContext
    {
        public GardenDesignerDbContext(): base("GardenDesignerDbContext")
        {
           
        }
        public DbSet<Structure> Structures { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectStructure> ProjectStructures { get; set; }
        public DbSet<CoordinatesXY> CoordinatesXY { get; set; }

    }
}