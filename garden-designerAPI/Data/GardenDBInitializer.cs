﻿using garden_designerAPI.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace garden_designerAPI.Data
{
    public class GardenDBInitializer : DropCreateDatabaseIfModelChanges<GardenDesignerDbContext>
    {
        protected override void Seed(GardenDesignerDbContext context)
        {
            IList<Structure> seededStructures = new List<Structure>()
            {
                new Structure(){ StructureId = 1, Name = "Oak", Type = "Tree"},
                new Structure(){ StructureId = 2, Name = "Apple-tree", Type = "Tree"},
                new Structure(){ StructureId = 3, Name = "Plum-Tree", Type = "Tree"},
                new Structure(){ StructureId = 4, Name = "Gooseberry", Type = "Bush"},
                new Structure(){ StructureId = 5, Name = "Currant", Type = "Bush"},
                new Structure(){ StructureId = 6, Name = "Raspberry", Type = "Bush"},
                new Structure(){ StructureId = 7, Name = "Grass", Type = "Other"},
                new Structure(){ StructureId = 8, Name = "Flower", Type = "Other"},
                new Structure(){ StructureId = 9, Name = "Reserved", Type = "Other"}
            };

            IList<Project> seededProjects = new List<Project>()
            {
                new Project(){ ProjectId = 1, Name = "Large garden project"}
            };
            
            context.Structures.AddRange(seededStructures);
            context.Projects.AddRange(seededProjects);

            base.Seed(context);
        }
    }
}